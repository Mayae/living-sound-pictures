﻿using UnityEngine;
using System.Collections;

public class WebcamAuth : MonoBehaviour {

    // Use this for initialization
    IEnumerator Start()
    {
        // yes, we actually need to do this through defines, even though Application.isWebPlayer exists (it isn't triggered for the editor
        // however, the webcam auth is still required).
        #if UNITY_WEBPLAYER
            yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
            if (Application.HasUserAuthorization(UserAuthorization.WebCam))
            {
                print("Authorized access to webcam...");
                GlobalData.instance().initWebcam();
                if (GameState.instance().onUserWebcamAuthorization != null)
                    GameState.instance().onUserWebcamAuthorization();
            }
            else
            {
                print("Denied access to webcam...");
            }

        #else
            GameState.instance().onGameLoad += onGameIsLoading;
        #endif
        yield break;
    }

    void onGameIsLoading()
    {
        print("Authorized access to webcam...");
        GlobalData.instance().initWebcam();
        if (GameState.instance().onUserWebcamAuthorization != null)
            GameState.instance().onUserWebcamAuthorization();
    }

    // Update is called once per frame
    void Update () {
	
	}
}
