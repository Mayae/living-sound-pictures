﻿using UnityEngine;
using System.Collections;
using Blunt.UnityExt;
public class CameraText : MonoBehaviour
{
    private Quaternion baseRotation;
    WebCamTexture text;
	// Use this for initialization

    void Start()
    {
        GameState.instance().onUserWebcamAuthorization += onWebCam;
    }

	void onWebCam()
    {
        text = GlobalData.instance().mainVideo;
        renderer.material.mainTexture = text;
        baseRotation = transform.rotation;
        if (Application.platform == RuntimePlatform.Android)
            baseRotation.eulerAngles = new Vector3(baseRotation.eulerAngles.x, baseRotation.eulerAngles.y, baseRotation.z - 90);
        float widthFactor = (float)text.width / text.height;
        //transform.localScale = new Vector3(transform.localScale.y * widthFactor, transform.localScale.y, transform.localScale.z);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(text != null)
            transform.rotation = baseRotation * Quaternion.AngleAxis(text.videoRotationAngle, Vector3.up);
	}
}
