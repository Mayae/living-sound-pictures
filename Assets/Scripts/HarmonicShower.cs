﻿
using UnityEngine;
using System.Collections;

public class HarmonicShower : MonoBehaviour
{
    public Color c1 = Color.yellow;
    public Color c2 = Color.red;
    public Transform parentBlock;
    public Material m;

    public VideoAnalysis display;

    public float lineWidth = 0.01f;

    public enum Channel
    {
        Red, Green, Blue
    }

    public Channel channelToVisualize;

    protected LineRenderer rasterizer;
    protected int harmonics;
    protected Transform tsf;
    

    protected bool isRasterizerEnabled;

    void Start()
    {
        harmonics = 16;
        isRasterizerEnabled = false;
        rasterizer = gameObject.AddComponent<LineRenderer>();
        if (m == null)
            rasterizer.material = new Material(Shader.Find("Mobile/Particles/Additive"));
        else
            rasterizer.material = m;
        rasterizer.SetColors(c1, c2);
        rasterizer.SetWidth(lineWidth, lineWidth);
        rasterizer.SetVertexCount(harmonics);
        rasterizer.useWorldSpace = false;


        tsf = GetComponent<Transform>();

        GameState.instance().onUpdate += onUpdate;
        if (parentBlock != null)
            transform.parent = parentBlock;
    }



    void onUpdate()
    {

    
        var recLength = 1.0f / harmonics;

        var ci = display.tsfImage;
        if (ci == null)
            return;


        switch(channelToVisualize)
        {
            case Channel.Red:
                for (int i = 0; i < harmonics; ++i)
                {
                    rasterizer.SetPosition(i, new Vector3(i * recLength - 0.5f, (float)ci.red[i]));
                }
                break;
            case Channel.Green:
                for (int i = 0; i < harmonics; ++i)
                {
                    rasterizer.SetPosition(i, new Vector3(i * recLength - 0.5f, (float)ci.green[i]));
                }
                break;
            case Channel.Blue:
                for (int i = 0; i < harmonics; ++i)
                {
                    rasterizer.SetPosition(i, new Vector3(i * recLength - 0.5f, (float)ci.blue[i]));
                }
                break;

        }

    }
}
