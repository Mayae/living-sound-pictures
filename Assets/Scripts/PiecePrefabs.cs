﻿using UnityEngine;
using System.Collections.Generic;
using Blunt.Probability.Music;
using Blunt.Synthesis;
using Blunt.Sound;
using Blunt.ImageSignalTransforms;
using System;

namespace PiecePrefabs
{

    using FMNote = Sequencer<FMSynth>.NoteInfo;
    using IMNote = Sequencer<ImageSynth>.NoteInfo;

    public abstract class Piece
    {
        static public List<Piece> getPieces()
        {
            hackC();
            return pieces;
        }

        static public void hackC()
        {
            Ping.hack = 1;
        }

        static private List<Piece> pieces = new List<Piece>();
        public string name, composer;
        public double signature;

        abstract public TimeSequence[] getChordSequences(int intensity);
        abstract public TimeSequence[] getMelodyOneSequences(int intensity);
        abstract public TimeSequence[] getMelodyTwoSequences(int intensity);

        abstract public IMNote[] harmonizeChords(PlayHead ph, ColourComponent mood, int rhythmIntensity, float intensity);
        abstract public FMNote[] harmonizeMelodyOne(PlayHead ph, ColourComponent mood, int rhythmIntensity, float intensity);
        abstract public FMNote[] harmonizeMelodyTwo(PlayHead ph, ColourComponent mood, int rhythmIntensity, float intensity);

        public Piece()
        {
            pieces.Add(this);
        }
    }


    class Ping : Piece
    {
        static private Ping register = new Ping();
        static public int hack = 0;
        const float volume = -9;

        public IMNote[][] redChords =
        {
            // C Major.
            new IMNote[]
            {
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.C, 3), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.E, 3), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.G, 3), volume, 2),
            },
            // A Minor.
            new IMNote[]
            {
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 2), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.C, 3), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.E, 3), volume, 2),
            },
            // F Major.
            new IMNote[]
            {
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 2), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 2), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.C, 3), volume, 2),
            },
            // G Major.
            new IMNote[]
            {
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.G, 2), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.B, 2), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 3), volume, 2),
            }
        };

        public IMNote[][] blueChords =
        {
            // A Minor.
            new IMNote[]
            {
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 2), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.C, 3), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.E, 3), volume, 2),
            },
            // C Major.
            new IMNote[]
            {
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.E, 2), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.GSharp, 3), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.B, 2), volume, 2),
            },

            // F Major.
            new IMNote[]
            {
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 2), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 2), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.C, 3), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.E, 3), volume, 2),
            },
            // G Major.
            new IMNote[]
            {
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.G, 2), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.B, 2), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 3), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 3), volume, 2),
            }
        };

        public IMNote[][] greenChords =
        {
            // Fb3-7.
            new IMNote[]
            {
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 2), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.C, 3), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.E, 3), volume, 2),
            },
            // Csus4
            new IMNote[]
            {
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.C, 3), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 3), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.G, 3), volume, 2),
            },

            // Csus2
            new IMNote[]
            {
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.C, 3), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 3), volume, 2),
                new IMNote(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.G, 3), volume, 2),
            }
        };

        private Ping()
        {
            name = "Ping";
            composer = "Janus Thorborg";
        }

        public override TimeSequence[] getChordSequences(int intensity)
        {
            if(intensity < 1)
            {
                return new TimeSequence[]
                {
                    new TimeSequence(new double[] { 0}, PlayHead.createTimeOffset(2, 0, 0, 0), 1, 8),
                };
            }
            else if(intensity < 3)
            {
                return new TimeSequence[]
                {
                    new TimeSequence(new double[] { 0, 4 }, PlayHead.createTimeOffset(2, 0, 0, 0), 1, 8),
                };
            }
            else if (intensity < 5)
            {
                return new TimeSequence[]
                {
                    new TimeSequence(new double[] { 0, 4, 2 }, PlayHead.createTimeOffset(2, 0, 0, 0), 1, 8),
                };
            }
            else
            {
                return new TimeSequence[]
                {
                    new TimeSequence(new double[] { 0, 2, 2, 2 }, PlayHead.createTimeOffset(0, 2, 0, 0), 1, 8),
                };
            }


        }

        public override TimeSequence[] getMelodyOneSequences(int intensity)
        {
            if(intensity < 3)
            {
                return new TimeSequence[]
                {
                    new TimeSequence(new double[] { 1.5 }, PlayHead.createTimeOffset(1, 0, 0, 0), 1)
                };
            }
            else
            {
                return new TimeSequence[]
                {
                    new TimeSequence(new double[] { 1.5, 2 }, PlayHead.createTimeOffset(1, 0, 0, 0), 1)
                };
            }

        }
        public override TimeSequence[] getMelodyTwoSequences(int intensity)
        {
            if (intensity < 2)
            {
                return new TimeSequence[]
                {
                    new TimeSequence(new double[] { 0}, PlayHead.createTimeOffset(1, 0, 0, 0), 1, 4)
                };
            }
            else if(intensity < 4)
            {
                return new TimeSequence[]
                {
                    new TimeSequence(new double[] { 0, 2 }, PlayHead.createTimeOffset(1, 0, 0, 0), 1, 4)
                };
            }
            else
            {
                return new TimeSequence[]
                {
                    new TimeSequence(new double[] { 0, 1, 1, 1}, PlayHead.createTimeOffset(1, 0, 0, 0), 1, 4)
                };
            }
        }

        public override IMNote[] harmonizeChords(PlayHead ph, ColourComponent mood, int rhythmIntensity, float lightIntensity)
        {



           /* double[] signal = { 0, 1, 0, -1 };

            double[] cmplx = new double[signal.Length * 2];

            for (int i = 0; i < cmplx.Length; i += 2)
                cmplx[i] = signal[i / 2];

            Lomont.LomontFFT f = new Lomont.LomontFFT();

            f.FFT(cmplx, true);

            for (int i = 0; i < cmplx.Length; i += 2)
                signal[i / 2] = Math.Sqrt(cmplx[i] * cmplx[i] + cmplx[i + 1] * cmplx[i + 1]);*/

            int barSignifier = ph.bars & 1;
            int dropBeat = ph.beats >> 1;
            int rbentry = ((barSignifier << 1) + dropBeat);
            switch (mood)
            {
                case ColourComponent.Red:
                    return redChords[rbentry];
                case ColourComponent.Green:
                    return greenChords[barSignifier + (dropBeat & barSignifier)];
                case ColourComponent.Blue:
                    return blueChords[rbentry];
                default:
                    break;
            }
            return null;
        }

        public override FMNote[] harmonizeMelodyOne(PlayHead ph, ColourComponent mood, int rhythmIntensity, float intensity)
        {
            var pitch = mood == ColourComponent.Blue ? EnvironmentTuning.Pitch.A : EnvironmentTuning.Pitch.G;
            return new FMNote[]
            {
                new FMNote(EnvironmentTuning.pitchFromComposite(pitch, 4), -9, 0.5f)
            };
        }
        public override FMNote[] harmonizeMelodyTwo(PlayHead ph, ColourComponent mood, int rhythmIntensity, float intensity)
        {
            if (rhythmIntensity == 0)
                return null;

            var pitch = (ph.beats >> 1) == 0 ? EnvironmentTuning.Pitch.D : EnvironmentTuning.Pitch.C;
            return new FMNote[]
            {
                new FMNote(EnvironmentTuning.pitchFromComposite(pitch, 5), -7, 0.5f)
            };
        }

    }
    
}
