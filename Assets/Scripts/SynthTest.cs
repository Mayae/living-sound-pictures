﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

#define V2

#define ADVANCED_SYNTH

using UnityEngine;
using System.Collections;
using Blunt;
using Blunt.Synthesis;
using Synth = Blunt.Synthesis.ImageSynth;


public class SynthTest : MonoBehaviour
{

    public VideoAnalysis videoSynthesis;

	private Synth mainSynth;
	public int downs = 0;
	public int ups = 0;
	public int activeVoices;
	public int activeOperators;
	public Blunt.Sound.Mixer mixer;

	// Use this for initialization
	public int numberOfVoices;
	class Tangent
	{
		public KeyCode keyCode;
		public int note;
		public Blunt.CQueue<Synth.Voice> voices;
		public Tangent(KeyCode kc, int n)
		{
			keyCode = kc;
			note = n;
			voices = new Blunt.CQueue<Synth.Voice>();
		}
	};

	readonly Tangent[] tangents =
	{
		new Tangent( KeyCode.A, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.C, 4)),
		new Tangent( KeyCode.W, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.CSharp, 4)),
		new Tangent( KeyCode.S, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.D, 4)),
		new Tangent( KeyCode.E, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.DSharp, 4)),
		new Tangent( KeyCode.D, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.E, 4)),
		new Tangent( KeyCode.F, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.F, 4)),
		new Tangent( KeyCode.T, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.FSharp, 4)),
		new Tangent( KeyCode.G, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.G, 4)),
		new Tangent( KeyCode.Y, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.GSharp, 4)),
		new Tangent( KeyCode.H, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.A, 4)),
		new Tangent( KeyCode.U, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.ASharp, 4)),
		new Tangent( KeyCode.J, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.B, 4)),
		new Tangent( KeyCode.K, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.C, 5))
	};

	class PlayableVoice
	{
		public readonly Synth.Voice voice;
		public Tangent associatedTangent;
		public PlayableVoice(Synth.Voice v)
		{
			voice = v;
			associatedTangent = null;
		}
	};
	private PlayableVoice[] voices;

    void Start()
    {
        GameState.instance().onGameIsLoaded += onGameIsLoaded;
        GameState.instance().onUpdate += onUpdate;
    }

	void onGameIsLoaded()
	{
        if(videoSynthesis == null)
        {
            mainSynth = new Synth();
        }
        else
        {
            mainSynth = videoSynthesis.getSynth();
        }
		var channel = mixer.getChannel("Synth Test");

		//channel.addStage(mainSynth);
		mixer.setIsEnabled(true);
		if(numberOfVoices < 1)
			numberOfVoices = 1;
		voices = new PlayableVoice[numberOfVoices];
		for(int i = 0; i < numberOfVoices; ++i)
		{
			voices[i] = new PlayableVoice((Synth.Voice)mainSynth.createVoice());
			var voice = voices[i].voice;

            voice.addOscillator(1.0f);
            voice.adsr.setADSR(10, 500, -12, 1000);

		}
        /*var lel = getAvailableOrOldestVoice();
        lel.voice.setPitch(20, 0);
        lel.voice.play(-6);*/
	}

	PlayableVoice getAvailableOrOldestVoice()
	{
		int oldIndex = 0;
		float oldTime = float.MaxValue;

		for(int i = 0; i < voices.Length; ++i)
		{

			if(!voices[i].voice.enabled)
			{
				return voices[i];
			}
			else if(oldTime > voices[i].voice.timeAtPlay)
			{
				oldTime = voices[i].voice.timeAtPlay;
				oldIndex = i;
			}
		}

		// no free voices found, return the oldest:

		return voices[oldIndex];
	}


	void onUpdate()
	{

		if(Input.GetKeyDown(KeyCode.Z))
		{
			for(int i = 0; i < tangents.Length; ++i)
			{
				// pitch 12 semitones down
				tangents[i].note -= 12;
			}

		}
		if(Input.GetKeyDown(KeyCode.X))
		{
			for(int i = 0; i < tangents.Length; ++i)
			{
				// pitch 12 semitones up
				tangents[i].note += 12;
			}

		}
		for(int i = 0; i < tangents.Length; ++i)
		{
			if(Input.GetKeyDown(tangents[i].keyCode))
			{
				var pv = getAvailableOrOldestVoice();
				pv.voice.setPitch(tangents[i].note, 0);
				pv.voice.stop();
				if(pv.associatedTangent != null)
				{
					pv.associatedTangent.voices.remove(pv.voice);
				}
				pv.associatedTangent = tangents[i];
				tangents[i].voices.push(pv.voice);
				pv.voice.play(-6f);
				downs++;

			}
			if(Input.GetKeyUp(tangents[i].keyCode))
			{
				var voice = tangents[i].voices.pop();
				// voice might have been stolen.
				if(voice != null)
				{
					voice.release();
				}
				//voices[i].release();
				ups++;
			}
		}

		activeVoices = mainSynth.numActiveVoices;
		activeOperators = mainSynth.numActiveOperators;
	}
}
