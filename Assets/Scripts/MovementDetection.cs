﻿using UnityEngine;
using System.Collections;

public class MovementDetection : MonoBehaviour
{
    public Diagnostics d;
    Vector3 vel;
    Vector3 gyro;

    float smoothed;
    float vels;

    bool isMoving;
	// Use this for initialization
	void Start ()
    {
        d.addDiagnostic(() => new Diagnostics.Message(Color.green, "vel.mag = " + vel));
        d.addDiagnostic(() => new Diagnostics.Message(isMoving ? Color.red : Color.green, "gyro.mag"  + GameState.instance().userMovementVelocity + " => " + isMoving.ToString()));
        GameState.instance().onUserMove += () => isMoving = true;
        GameState.instance().onUserStill += () => isMoving = false; ;
        Input.gyro.enabled = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
        vel = Input.acceleration;
        gyro = Input.gyro.userAcceleration;

        smoothed = Mathf.SmoothDamp(smoothed, gyro.magnitude, ref vels, 0.8f);
        GlobalData.instance().smoothedVelocity = smoothed;
	}
}
