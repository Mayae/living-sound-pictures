﻿using UnityEngine;
using System.Collections;
using Blunt.ImageSignalTransforms;

public class GlobalData
{

    public delegate void EventAction();

    public EventAction onDataUpdate;
    public EventAction onMusicChange;

    public int numHarmonics = 15;

    public float smoothedVelocity;
    public float intensity;
    public int dominantImageFrequency;
    public ColourImageCoeffs1D imageSynthTexture;
    public FloatColour colourValueDeviations;
    public ColourComponent dominantColour;
    public float colourSaturation;
    public MusicEntry currentMusicPiece;

    public WebCamTexture mainVideo
    {
        get; private set;
    }

    public bool videoDidUpdate
    {
        get { return mainVideo.didUpdateThisFrame; }
    }

    private static GlobalData internalInstance;

    public static void fieldsUpdated()
    {
        if (instance().onDataUpdate != null)
            instance().onDataUpdate();
    }

    public static void musicChanged()
    {
        if (instance().onMusicChange != null)
            instance().onMusicChange();
    }

    public static GlobalData instance()
    {
        if (internalInstance == null)
        {
            internalInstance = new GlobalData();
        }
        return internalInstance;
    }

    public void initWebcam()
    {
        // initialize camera
        Debug.Log("Creating webcam texture..");
        try
        {
            mainVideo = new WebCamTexture(WebCamTexture.devices[0].name);
        }
        catch(System.Exception e)
        {
            Debug.Log("Error creating webcam texture: " + e.Message);
        }
        mainVideo.requestedFPS = 60;
        mainVideo.requestedWidth = 180;
        mainVideo.requestedHeight = 320;
        mainVideo.Play();
        Debug.Log("Globals: Loaded webcam " + WebCamTexture.devices[0].name + " at " + mainVideo.width + "x" + mainVideo.height);
    }

    private GlobalData()
    {



    }


}

