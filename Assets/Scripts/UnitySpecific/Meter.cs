﻿#define HAS_UNITY
#if HAS_UNITY

using UnityEngine;
using System.Collections;
namespace Blunt
{
    public class Meter : MonoBehaviour
    {
        public Color c1 = Color.yellow;
        public Color c2 = Color.red;
        public Transform parentBlock;
        public Material m;


        public float lineWidth = 0.01f;

        protected LineRenderer rasterizer;
        protected Transform tsf;

        protected bool isRasterizerEnabled;


        public float meterValue = 1.0f;

        void Start()
        {
            isRasterizerEnabled = false;
            rasterizer = gameObject.AddComponent<LineRenderer>();
            if (m == null)
                rasterizer.material = new Material(Shader.Find("Mobile/Particles/Additive"));
            else
                rasterizer.material = m;
            rasterizer.SetColors(c1, c2);
            rasterizer.SetWidth(lineWidth, lineWidth);
            rasterizer.SetVertexCount(2);
            rasterizer.useWorldSpace = false;


            tsf = GetComponent<Transform>();

            GameState.instance().onUpdate += onUpdate;
            if (parentBlock != null)
                transform.parent = parentBlock;
        }

        public void setValue(float input)
        {
            meterValue = Mathf.Clamp01(input);
        }

        public void setColour(Color color)
        {
            c1 = c2 = color;
            rasterizer.SetColors(color, color);
        }

        void onUpdate()
        {
            rasterizer.SetPosition(0, new Vector3(0, 0 - 0.5f));

            rasterizer.SetPosition(1, new Vector3(0, meterValue - 0.5f));
        }
    }
}

#endif