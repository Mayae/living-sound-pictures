﻿#define HAS_UNITY
#if HAS_UNITY

using UnityEngine;
using System.Collections;
namespace Blunt
{
    public class LineGraph : MonoBehaviour
    {
        public Color c1 = Color.yellow;
        public Color c2 = Color.red;
        public Transform parentBlock;
        public Material m;

        public bool isComplex;
        public bool isSpectrum;
        public bool doScale;

        public float lineWidth = 0.01f;

        protected LineRenderer rasterizer;
        protected Transform tsf;

        protected bool isRasterizerEnabled;

        IndirectIterator.IBase iteratorCreater;

        private int vertexCount;
        void Start()
        {
            isRasterizerEnabled = false;
            rasterizer = gameObject.AddComponent<LineRenderer>();
            if (m == null)
                rasterizer.material = new Material(Shader.Find("Mobile/Particles/Additive"));
            else
                rasterizer.material = m;
            rasterizer.SetColors(c1, c2);
            rasterizer.SetWidth(lineWidth, lineWidth);
            rasterizer.SetVertexCount(10);
            rasterizer.useWorldSpace = false;


            tsf = GetComponent<Transform>();

            GameState.instance().onUpdate += onUpdate;
            if (parentBlock != null)
                transform.parent = parentBlock;
        }

        public void setData(IndirectIterator.IBase creater)
        {
            iteratorCreater = creater;
        }


        void onUpdate()
        {
            if (iteratorCreater == null)
                return;

            var iterator = iteratorCreater.create();

 
            if (iterator.size == 0)
            {
                return;
            }




            if(!isComplex)
            {
                if (iterator.size != vertexCount)
                {
                    vertexCount = (int)iterator.size;
                    rasterizer.SetVertexCount(vertexCount);
                }

                var recLength = 1.0f / vertexCount;

                float scale = doScale ? recLength : 1.0f;

                for (int i = 0; i < vertexCount; ++i)
                {
                    iterator.advance();
                    rasterizer.SetPosition(i, new Vector3(i * recLength - 0.5f, scale * iterator.get<float>()));
                }
            }
            else
            {
                if (iterator.size != vertexCount)
                {
                    vertexCount = (int)(iterator.size >> 1);
                    if (isSpectrum)
                        vertexCount >>= 1;
                    rasterizer.SetVertexCount(vertexCount);
                }

                var recLength = 1.0f / (vertexCount - 1);

                float scale = doScale ? recLength : 1.0f;

                for (int i = 0; i < vertexCount; i++)
                {
                    iterator.advance();
                    float real = iterator.get<float>();
                    iterator.advance();
                    float imag = iterator.get<float>();
                    float magnitude = Mathf.Sqrt(real * real + imag * imag);

                    rasterizer.SetPosition(i, new Vector3(i * recLength - 0.5f, scale * magnitude));
                }
            }


        }
    }
}

#endif