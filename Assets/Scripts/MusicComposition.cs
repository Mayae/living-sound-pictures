﻿using UnityEngine;
using System.Collections.Generic;

using Blunt.Synthesis;
using Blunt.Sound.GenerativeMusic;
using Blunt.Probability.Music;

public class MusicEntry
{
    PiecePrefabs.Piece generator;
    MusicComposition composition;
    SequencerSystem<FMSynth> melody;
    SequencerSystem<ImageSynth> chords;

    public float timeAtLastPlay { get; private set; }
    public string composer { get { return generator.composer; } }
    public string name { get { return generator.name; } }
    int oldIntensity;

    public float elapsedTime()
    {
        return GameState.instance().gameTime - timeAtLastPlay;
    }

    public MusicEntry(PiecePrefabs.Piece gen, SequencerSystem<FMSynth> melodySystem, SequencerSystem<ImageSynth> chordsSystem, GenericVoiceBuffer melodyVoices, GenericVoiceBuffer chordVoices)
    {
        // copy variables over, and instantiate music composition
        generator = gen;
        melody = melodySystem;
        chords = chordsSystem;
        composition = new MusicComposition(generator.name, generator.composer, melody, chords);

        RandomSequenceList
            crsl = new RandomSequenceList(),
            m1rsl = new RandomSequenceList(),
            m2rsl = new RandomSequenceList();

        // fill the patterns for each intensity level
        for (int i = 0; i < 8; ++i)
        {
            crsl.switchToBank(i);
            m1rsl.switchToBank(i);
            m2rsl.switchToBank(i);

            crsl.addTimeSequences(generator.getChordSequences(i));
            m1rsl.addTimeSequences(generator.getMelodyOneSequences(i));
            m2rsl.addTimeSequences(generator.getMelodyTwoSequences(i));
        }

        // reset banks so we start at intensity zero
        crsl.switchToBank(0);
        m1rsl.switchToBank(0);
        m2rsl.switchToBank(0);
        var g = GlobalData.instance();
        // setup the instruments and harmonizers using the voice buffers
        composition.addChordInstrument(chordVoices, crsl,
            ph =>  generator.harmonizeChords(ph, g.dominantColour, g.dominantImageFrequency, g.intensity)
        );

        composition.addMelodyInstrument(melodyVoices, m1rsl,
            ph => generator.harmonizeMelodyOne(ph, g.dominantColour, g.dominantImageFrequency, g.intensity)
        );

        composition.addMelodyInstrument(melodyVoices, m2rsl,
            ph => generator.harmonizeMelodyTwo(ph, g.dominantColour, g.dominantImageFrequency, g.intensity)
        );

        // hook data change
        GlobalData.instance().onDataUpdate += parameterHook;
    }

    public void play()
    {
        timeAtLastPlay = GameState.instance().gameTime;
        composition.play();
    }

    public void stop()
    {
        composition.stop();
    }

    /// <summary>
    /// called when input feeds and data has changed, we now have an opportunity to change
    /// stuff in the composition.
    /// </summary>
    void parameterHook()
    {
        int newIntensity = GlobalData.instance().dominantImageFrequency;
        if (newIntensity != oldIntensity)
        {
            composition.switchToRhythmicBank(newIntensity);
            oldIntensity = newIntensity;
        }
        // refresh synthesis texutre.
        chords.synth.resynthesize(GlobalData.instance().imageSynthTexture);

    }
}

public class MusicComposition
{
    MusicPiece<FMSynth> melodyPiece;
    MusicPiece<ImageSynth> chordPiece;
    SequencerSystem<FMSynth> melodyRenderer;
    SequencerSystem<ImageSynth> chordRenderer;

    List<RandomSequenceList> sequences;

    string name;
    string composer;

    public MusicComposition(string pieceName, string pieceComposer, SequencerSystem<FMSynth> melody, SequencerSystem<ImageSynth> chords)
    {
        name = pieceName;
        composer = pieceComposer;
        melodyRenderer = melody;
        chordRenderer = chords;
        melodyPiece = new MusicPiece<FMSynth>(melody.sequencer, melody.callbackSequencer, melody.synth);
        chordPiece = new MusicPiece<ImageSynth>(chords.sequencer, chords.callbackSequencer, chords.synth);
        sequences = new List<RandomSequenceList>();
    }

    public void play()
    {
        if (!melodyPiece.isCurrentlyPlaying())
        {
            melodyPiece.playPiece(0);
            chordPiece.playPiece(0);
        }

    }

    public void stop()
    {
        if (melodyPiece.isCurrentlyPlaying())
        {
            melodyPiece.stopPiece(0);
            chordPiece.stopPiece(0);
        }
    }


    public void switchToRhythmicBank(int bankNumber)
    {
        foreach (var s in sequences)
            s.switchToBank(bankNumber);
    }

    public MusicPiece<FMSynth>.Instrument addMelodyInstrument(GenericVoiceBuffer voiceBuf, RandomSequenceList rhythm,
        System.Func<PlayHead, Sequencer<FMSynth>.NoteInfo[]> harmonicMusicGenerator)
    {
        MusicPiece<FMSynth>.Instrument inst;
        inst = melodyPiece.createAndAddInstrument(voiceBuf, rhythm, harmonicMusicGenerator);
        sequences.Add(inst.rsl);
        return inst;
    }

    public MusicPiece<ImageSynth>.Instrument addChordInstrument(GenericVoiceBuffer voiceBuf, RandomSequenceList rhythm,
            System.Func<PlayHead, Sequencer<ImageSynth>.NoteInfo[]> harmonicMusicGenerator)
    {
        MusicPiece<ImageSynth>.Instrument inst;
        inst = chordPiece.createAndAddInstrument(voiceBuf, rhythm, harmonicMusicGenerator);
        sequences.Add(inst.rsl);
        return inst;
    }
}
