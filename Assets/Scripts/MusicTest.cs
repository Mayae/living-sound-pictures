﻿using UnityEngine;
using System.Collections;
using Blunt;
using Blunt.Synthesis;
using Blunt.Sound;
using System.Collections.Generic;
using Blunt.Probability.Music;

public class MusicTest : MonoBehaviour
{
    public Mixer mixer;
    public int timeBeforeNewPiece = 10;
    private Mixer.MixerChannel channel;

    VoiceBuffer<FMSynth.Voice> melodyVoices;
    VoiceBuffer<ImageSynth.Voice> chordVoices;
    Blunt.Utils.CheapRandom ran = new Utils.CheapRandom();

    SequencerSystem<ImageSynth> imageRenderer;
    SequencerSystem<FMSynth> synthRenderer;

    List<MusicEntry> pieces;


    int pieceEntry = -1;

    MusicEntry currentPiece;

    public int imageVoices;
    public int fmVoices;

    void playNextPiece()
    {
        if (pieces.Count < 1)
        {
            print("No music numbers available.");
            return;
        }


        pieceEntry++;
        pieceEntry %= pieces.Count;
        pieces[pieceEntry].play();
        currentPiece = pieces[pieceEntry];
        GlobalData.instance().currentMusicPiece = currentPiece;
        print("Playing number: \"" + currentPiece.name + "\" by " + currentPiece.composer);
        GlobalData.musicChanged();
    }

    void onUserStill()
    {
        if(currentPiece != null)
        {
            if (currentPiece.elapsedTime() > timeBeforeNewPiece)
                playNextPiece();
        }
        else
        {
            playNextPiece();
        }
    }

    void onAudioLoad()
    {
        imageRenderer = new SequencerSystem<ImageSynth>();
        synthRenderer = new SequencerSystem<FMSynth>();
        pieces = new List<MusicEntry>();

        channel = mixer.getChannel("Music");

        melodyVoices = new VoiceBuffer<FMSynth.Voice>(synthRenderer.synth);
        chordVoices = new VoiceBuffer<ImageSynth.Voice>(imageRenderer.synth);

        melodyVoices.initialize(10,
            v =>
            {
                FMSynth.Voice.Operator op;
                op = v.createAndAddOperator();
                op.envelope.setADSR(10, 500, -24, 1000);
            }
        );

        chordVoices.initialize(10,
            v =>
            {
                v.adsr.setADSR(500, 2500, -10, 2000);
                v.addOscillator(1, 1);
            }
        );

        imageRenderer.addToMixerChannel(channel);
        synthRenderer.addToMixerChannel(channel);

        mixer.setIsEnabled(true);
        channel.setIsEnabled(true);

        // register all static prefabricated pieces
        foreach (var p in PiecePrefabs.Piece.getPieces())
            pieces.Add(new MusicEntry(p, synthRenderer, imageRenderer, melodyVoices, chordVoices));

    }


	// Use this for initialization
	void Start ()
    {
        GameState.instance().onAudioLoad += onAudioLoad;
        GameState.instance().onUpdate += onUpdate;
        //GameState.instance().onUserStill += playNextPiece;
        GameState.instance().onGameIsLoaded += playNextPiece;
	}
	
	// Update is called once per frame
	void onUpdate ()
    {
        imageVoices = imageRenderer.synth.numActiveVoices;
        fmVoices = synthRenderer.synth.numActiveVoices;
	}
}
