﻿using UnityEngine;
using System.Collections;
using Blunt;
using Blunt.Synthesis;
using Blunt.Sound;
using Blunt.ImageSignalTransforms;
using Blunt.UnityExt;

public class VideoAnalysis : MonoBehaviour
{
    public Mixer mixer;
    public Diagnostics diag;
    public LineGraph redChannel;
    public LineGraph fftGraphic;
    public Block videoAnalysisArea;
    private Mixer.MixerChannel channel;
    public Metronome metro;
    private ImageSynth imageSynth;
    public CameraText videoFeed;
    public Meter redValue, greenValue, blueValue, majorComponent;

    Utils.CheapRandom ran = new Utils.CheapRandom();
    ColourBoxFilterKernel1D.BoxFilterSettings horizontalFilter, verticalFilter;
    Sequencer<FMSynth> s;
    Sequencer<FMSynth>.SequencerToken st;
    WebCamTexture texture;
    Texture2D staticTexture;
    Color32[] image;
    FourierTransform1D fft;
    public ColourImageCoeffs1D tsfImage, blobImage;
    public ImageCoeffs1D patternImage;
    float lastTime;
    public float videoFps;
    float maxDivision;

    public int div;
    private float animationPhase;
    public bool frozen = false;
    public bool animating = true;

    public ImageSynth getSynth()
    {
        if(imageSynth == null)
        {
            print("shit");

        }
        return imageSynth;
    }

    void onAudioLoad()
    {

        imageSynth = new ImageSynth();
        imageSynth.setLatency(2048);

        channel = mixer.getChannel("Main");
        channel.addStage(imageSynth);

        mixer.setIsEnabled(true);
        channel.setIsEnabled(true);

    }


    void onWebcamLoad()
    {
        texture = GlobalData.instance().mainVideo;

        lastTime = Time.time;
        print("Webcam texture for video analysis loaded.. Runtime platform is " + Application.platform.ToString());
        tsfImage = new ColourImageCoeffs1D();
        patternImage = new ImageCoeffs1D();
        blobImage = new ColourImageCoeffs1D();
        patternImage.bufferSize = 16;
        tsfImage.bufferSize = 16;
        blobImage.bufferSize = 16;
        verticalFilter = new ColourBoxFilterKernel1D.BoxFilterSettings();
        horizontalFilter = new ColourBoxFilterKernel1D.BoxFilterSettings();


        updateTextures();

        fft = new FourierTransform1D();

        redChannel.setData(new CRCIteratorBase<double>(() => tsfImage.red));
        fftGraphic.setData(new CRCIteratorBase<double>(() => patternImage.ftIntensity));

        
    }
    
    void updateTextures()
    {
        print("Updating textures (" + texture.width + ", " + texture.height + ")");
        image = new Color32[texture.width * texture.height];

        verticalFilter.kernelSize = 0.1f;
        verticalFilter.imageWidth = (uint)texture.width;
        verticalFilter.imageHeight = (uint)texture.height;
        verticalFilter.swapAxis = (Application.platform == RuntimePlatform.Android) || Application.isMobilePlatform;
        verticalFilter.offsetAxis = -1f;

        horizontalFilter.kernelSize = 0.3f;
        horizontalFilter.imageHeight = verticalFilter.imageHeight;
        horizontalFilter.imageWidth = verticalFilter.imageWidth;
        horizontalFilter.swapAxis = !verticalFilter.swapAxis;
        horizontalFilter.offsetAxis = 0.0f;
        staticTexture = new Texture2D(texture.width, texture.height);
    }


	// Use this for initialization
	void Start ()
    {
        GameState.instance().onAudioLoad += onAudioLoad;
        GameState.instance().onUpdate += onUpdate;
        GameState.instance().onUserWebcamAuthorization += onWebcamLoad;
        GameState.instance().onGameIsLoaded +=
            () =>
            {
                diag.addDiagnostic(() => new Diagnostics.Message(Color.green, "Video FPS: " + videoFps.ToString()));
                diag.addDiagnostic(() => new Diagnostics.Message(Color.green, "Synth FPS: " + imageSynth.synthesisFps));
                diag.addDiagnostic(() => new Diagnostics.Message(Color.green, "Freq Div: 1/" + maxDivision));

                mixer.enableSoundSystem();
            };
        GameState.instance().onMutedAudio += updateMetroSpeed;
    }
	
    void updateMetroSpeed(float[] something, int blah)
    {
        if(patternImage != null && patternImage.ftIntensity != null)
        {
            var data = patternImage.ftIntensity.getData();
            double mmag = System.Math.Sqrt(data[2] * data[2] + data[3] * data[3]);

            float maxIndice = 2;

            for(int i = 4; i < (patternImage.ftIntensity.size() / 2); i += 2)
            {
                double nmag = System.Math.Sqrt(data[i] * data[i] + data[i + 1] * data[i + 1]);
                if(nmag > mmag)
                {
                    mmag = nmag;
                    maxIndice = i;
                }
            }

            maxIndice /= 2;

            maxDivision = maxIndice;

            //metro.changeBaseSpeed(1.0f / maxIndice);
        }
    }

    void simpleBlobAnalysis()
    {

    }

    public void onFreezeButton()
    {
        frozen = !frozen;
        if (frozen)
        {
            staticTexture.SetPixels32(image);
            staticTexture.Apply();
            videoFeed.renderer.material.mainTexture = staticTexture;
        }
        else
        {
            videoFeed.renderer.material.mainTexture = GlobalData.instance().mainVideo;
        }
    }

    public void onAnimButton()
    {
        animating = !animating;

    }

    // Update is called once per frame
    void onUpdate ()
    {
	    if(texture != null && image != null && GlobalData.instance().videoDidUpdate)
        {
            videoFps = 1.0f / (Time.time - lastTime);
            if(!frozen)
            {
                // fucking annoying bug on OS X
                if(texture.width * texture.height != image.Length)
                {
                    updateTextures();
                }

                texture.GetPixels32(image);
            }



            // analyse the colours for the additive synth:
            ColourBoxFilterKernel1D.analyse(image, tsfImage, verticalFilter);
            if(animating)
            {
                animationPhase += Time.deltaTime * 0.2f * 2 * Mathf.PI;
                verticalFilter.offsetAxis = Mathf.Sin(animationPhase);
            }


            videoAnalysisArea.setWidth(verticalFilter.kernelSize);

            float areaOffset = verticalFilter.kernelSize * 0.5f + (verticalFilter.offsetAxis * 0.5f + 0.5f) * (1 - verticalFilter.kernelSize);

            videoAnalysisArea.transform.localPosition = videoAnalysisArea.transform.localPosition.withX(areaOffset - 0.5f);


            //imageSynth.resynthesize(tsfImage);
            GlobalData.instance().imageSynthTexture = tsfImage;
            // analyse the colours for blob analysis, or rather, the major dominant colour component
            ColourBoxFilterKernel1D.analyse(image, blobImage, horizontalFilter);
            var deviations = ColourAnalysis.rgbDeviation(blobImage);

            var devNorm = deviations;

            devNorm.normalize();

            deviations = deviations * 0.75f + devNorm * 0.25f;
            GlobalData.instance().colourValueDeviations = deviations;


            // set the colour saturation.
            GlobalData.instance().colourSaturation = (deviations.red + deviations.blue + deviations.green) * 0.5f;
            var color = GlobalData.instance().dominantColour = ColourAnalysis.majorColourComponent(blobImage);

            majorComponent.setColour(color == ColourComponent.Red ? Color.red : color == ColourComponent.Green ? Color.green : Color.blue);

            redValue.setValue(deviations.red);
            blueValue.setValue(deviations.blue);
            greenValue.setValue(deviations.green);

            // analyse the image for pattern recognition
            ColourBoxFilterKernel1D.analyse(image, patternImage, horizontalFilter);
            fft.forward1D(patternImage);

            GlobalData.instance().dominantImageFrequency = (int)(maxDivision - 1);

            lastTime = Time.time;

            GlobalData.fieldsUpdated();
        }

	}
}
