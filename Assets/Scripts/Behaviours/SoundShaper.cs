﻿using UnityEngine;
using System.Collections;
using Blunt.Sound;
using Blunt.ImageSignalTransforms;

public class SoundShaper : MonoBehaviour
{

    PhaserEffect phaser;
    ValveEffect valve;
    Schroeder reverb;
    public ColourComponent colour;
    private ColourComponent internalColour;
    public float effCutoff = 0.0001f;
    struct FState
    {
        public float current, target, velocity, time;

        public void filter()
        {
            current = Blunt.MathExt.sanitize01(Mathf.SmoothDamp(current, Blunt.MathExt.sanitize01(target), ref velocity, time));
        }


        public void setTime(float timeToSmooth)
        {
            time = timeToSmooth;
        }

    }

    FState red, green, blue;




    void changeSoundColour(ColourComponent sc)
    {
        switch(sc)
        {
            case ColourComponent.Red: red.target = 1.0f; green.target = 0.0f; blue.target = 0.0f; break;
            case ColourComponent.Green: green.target = 1.0f; red.target = 0.0f; blue.target = 0.0f; break;
            case ColourComponent.Blue: blue.target = 1.0f; red.target = 0.0f; green.target = 0.0f; break;
        }

        colour = internalColour = sc;
    }

    void filter()
    {
        var g = GlobalData.instance();
        red.target = g.colourValueDeviations.red;
        blue.target = g.colourValueDeviations.blue;
        green.target = g.colourValueDeviations.green;

        red.filter(); green.filter(); blue.filter();
    }

	// Use this for initialization
	void Start ()
    {
        phaser = GetComponent<PhaserEffect>();
        valve = GetComponent<ValveEffect>();
        reverb = GetComponent<Schroeder>();

        if(phaser == null || valve == null || reverb == null)
        {
            print("Error in composition; some effects could not be found.");
        }

        red.setTime(0.9f);
        blue.setTime(0.96f);
        green.setTime(0.9f);


        changeSoundColour(ColourComponent.Green);
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(colour != internalColour)
        {
            changeSoundColour(colour);
        }

        filter();

        valve.mix = red.current;

        reverb.mix = blue.current;
        phaser.depth = green.current;
        /*if (green.current < effCutoff)
            phaser.isEnabled = false;
        else
            phaser.isEnabled = true;

        if (red.current < effCutoff)
            valve.enabled = false;
        else
            valve.enabled = true;*/


	}
}
