﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Blunt.Synthesis;
namespace Blunt.Probability
{
	namespace Music
	{
		public class TimeSequence
		{
			public double[] sequence;
			public PlayHead.TimeOffset quantization;
            /// <summary>
            /// The minimum length of this pattern.
            /// </summary>
            public double length;
			/// <summary>
			/// The chance to choose this piece.
			/// </summary>
			public float chance;
			/// <summary>
			/// The likelihood of choosing the preferredNext sequence
			/// </summary>
			public float preferability;
			/// <summary>
			/// If non-null, this next sequence will be more likely to play.
			/// </summary>
			public TimeSequence preferredNext;
			public TimeSequence(double[] s, PlayHead.TimeOffset q, float chanceToPlay, double minLengthInBeats = 0, TimeSequence next = null, float nextPref = 0.0f)
			{
                double slen = 0;
                foreach (double b in s)
                    slen += b;

                length = System.Math.Max(slen, minLengthInBeats);

				sequence = s; quantization = q;
				chance = chanceToPlay;
				preferredNext = next;
				preferability = nextPref;
			}
		}

		public class RandomSequenceList
		{
			struct PatternDescriptor
			{
				public TimeSequence rhythm;
				public int where;
                public double when;
				public void reset()
				{
					where = 0;
				}

				public PatternDescriptor(TimeSequence s)
				{
					rhythm = s;
					where = 0;
                    when = 0;
				}
			}
			List<PatternDescriptor> pieces;
            Dictionary<int, List<PatternDescriptor>> banks = new Dictionary<int, List<PatternDescriptor>>();

            CResizableContainer<float> chances = new CResizableContainer<float>();
			PatternDescriptor current;
			Utils.CheapRandom dice = new Utils.CheapRandom();
			double offset;

			public void chooseSequence(PlayHead p, double addedOffset = 0)
			{
				if(current.rhythm != null)
                {
                    current.reset();

                    if (current.rhythm.preferredNext != null)
                    {
                        // firstly, dice roll to see if we should choose the preferred next.
                        var chance = dice.random01();
                        if (current.rhythm.preferability > chance)
                        {
                            current = new PatternDescriptor(current.rhythm.preferredNext);
                            current.when = p.preciseTiming + addedOffset;
                            return;
                        }
                    }
                }

				// okay, so now we just choose one at "random".
				current = pieces[dice.chooseFromChances(chances.getData())];
                current.when = p.preciseTiming + addedOffset;
            }

            public RandomSequenceList()
            {
                switchToBank(0);
            }

            /// <summary>
            /// Switches to the selected bank. The bank will be allocated if it doesn't exist.
            /// The effect takes place after the current sequence has been played, after which 
            /// new sequences will be played from the new bank.
            /// </summary>
            /// <param name="bankNumber"></param>
            public void switchToBank(int bankNumber)
            {
                if (!banks.ContainsKey(bankNumber))
                    banks.Add(bankNumber, new List<PatternDescriptor>());
                pieces = banks[bankNumber];
                updateChances();
            }

			/// <summary>
			/// Copies over the chances from all pieces into chances.
			/// After this call, each piece has a corrosponding normalized chance
			/// in chances.getData()
			/// </summary>
			void updateChances()
			{
				chances.resize(pieces.Count);
				var cbuf = chances.getData();
				float magnitude = 0;
				// copy over chances.
				for(int i = 0; i < pieces.Count; ++i)
				{
					cbuf[i] = pieces[i].rhythm.chance;
					magnitude += pieces[i].rhythm.chance;
				}
				// rescale magnitude
				magnitude = 1.0f / magnitude;
				for(int i = 0; i < pieces.Count; ++i)
				{
					// normalize chances
					cbuf[i] *= magnitude;
				}
			}

			public void resetSequencer()
			{
				current.where = 0;
				current.rhythm = null;
			}

			public void addTimeSequence(TimeSequence s)
			{
                if (s == null)
                    return;
                pieces.Add(new PatternDescriptor(s));
				updateChances();
			}

			public void addTimeSequences(TimeSequence[] s)
			{
                if (s == null)
                    return;

				foreach(var t in s)
					pieces.Add(new PatternDescriptor(t));
				updateChances();
			}
			/// <summary>
			/// Use this to support patterns that don't start perfectly on zero.
			/// </summary>
			/// <returns></returns>
			public bool isStartAndNotZero(PlayHead p)
			{
				if(pieces.Count == 0)
				{
					Debug.Log("Warning! Empty randomized sequence is being peeked.");
					return false;	// seems sensible.
				}
				// have to choose a sequence
				if(current.rhythm == null)
				{
					chooseSequence(p);
					// hack
					offset = p.getDistanceToNextBar(0, 0, 0, (float)current.rhythm.quantization.beats).beats;
				}
				if(current.where == 0 && current.rhythm.sequence[0] != 0.0)
					return true;
				else
					return false;
			}

			public double getNextMoment(PlayHead p)
			{
				if(pieces.Count == 0)
				{
					Debug.Log("Warning! Empty randomized sequence is being sequenced.");
					return 1;   // seems sensible.
				}

				if(current.rhythm == null || current.where == current.rhythm.sequence.Length)
				{
                    // calculate the remaining time of the current pattern. It may be the cumulated time since it started
                    // is actually larger than the pattern length, thus cutting it off at zero.
                    double remainingTime = System.Math.Max(0, (current.rhythm.length + current.when) - p.preciseTiming);
                    float currentQ = (float)current.rhythm.quantization.beats;
                    chooseSequence(p/*, remainingTime*/);
					offset = PlayHead.getDistanceToNextBar(p.preciseTiming/* + remainingTime*/, 0, 0, 0, (float)current.rhythm.quantization.beats).beats;
				}
				double value = offset + current.rhythm.sequence[current.where];
                current.where++;
                // avoid emitting zero's
                if (value == 0.0 && current.where == current.rhythm.sequence.Length)
                {
                    value += current.rhythm.length;
                }

				offset = 0;
				return value;
			}

		}
	}


}