/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

#define BLUNT_DEBUG

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Blunt
{
	namespace Sound
	{
		using DSPType = System.Single;
		using SystemFloat = System.Single;

		public class ValveEffect : MonoBehaviour, MonoFilter
		{
            // don't we just love single inheritance, 
            // forcing us to reimplement nearly everything 
            // as composition.. ?
            public DSPType gain = 1.0f;
            public DSPType mix = 1.0f;
            public DSPType rollOff;
			private Valve effect = new Valve();
            public bool enabled;

            public FilterPlugin getFilter() { return effect; }

			public void OnAudioFilterRead(SystemFloat[] data, int nChannels)
			{
                effect.lf.designLP(rollOff);
                effect.rf.designLP(rollOff);
                effect.gain = gain;
                effect.setMix(mix);
                effect.enable(enabled);
				effect.process(data, data.Length / nChannels, nChannels, false);

            }
		}
		/// <summary>
		/// Limiting is basically a special case of 		
		/// compression, where your ratio is infinite and   
		/// your attack is zero.
		/// </summary>
		public class Valve : FilterPlugin
		{
            private DSPType mix;
            public DSPType gain;
            public Synthesis.Filters.OnePole lf, rf;

            /// <summary>
            ///  actual precision doesn't really matter that much... its a dc blocker
            /// </summary>
            private float dcb1f = (float)Math.Exp(-2 * Math.PI * 5 / 44100);
            private float dca0f = 1.0f - (float)Math.Exp(-2 * Math.PI * 5 / 44100);

            private float dclz1f, dcrz1f;

            public bool enabled;
            public void enable(bool doEnable) { enabled = doEnable; }
            public void setMix(DSPType _mix) { mix = Mathf.Clamp01(_mix); }
            public void calculateCoefficients() { }
			// utility
			public DSPType currentGain;
			public void process(SystemFloat[] data, int nSampleFrames, int channels, bool channelIsEmpty)
			{
                if (!enabled)
                    return;

                int bufsize = nSampleFrames * channels;

                DSPType s1, s2, es1, es2;
                DSPType 
                    // lowpass coefficients
                    lz1 = lf.z1, rz1 = rf.z1, a0 = lf.a0, b1 = lf.b1, 
                    // dc block coefficients
                    dclz1 = dclz1f, dcrz1 = dcrz1f, dca0 = dca0f, dcb1 = dcb1f;

                DSPType scale = 1.0f / (2f + gain / 5);

                for (int i = 0; i < bufsize; i += channels)
				{
					s1 = data[i];
					s2 = data[i + 1];

                    // lowpass
                    lz1 = s1 * a0 + lz1 * b1;
                    rz1 = s2 * a0 + rz1 * b1;

                    // preamp
                    es1 = lz1 * gain;
                    es2 = rz1 * gain;

                    // valve drive
                    es1 = es1 > 0.0f ? (float)System.Math.Atan(es1) : es1;
                    es2 = es2 > 0.0f ? (float)System.Math.Atan(es2) : es2;

                    // master
                    es1 *= scale;
                    es2 *= scale;

                    // dc block

                    dclz1 = es1 * dca0 + dclz1 * dcb1;
                    dcrz1 = es2 * dca0 + dcrz1 * dcb1;

                    // mix
                    data[i] = (es1 - dclz1) * mix + s1 * (1 - mix);
					data[i + 1] = (es2 - dcrz1) * mix + s2 * (1 - mix);

                    

                }
                // save lowpass
                lf.z1 = lz1; rf.z1 = rz1;
                // save highpass
                dclz1f = dclz1; dcrz1f = dcrz1;
            }


		};

	}
}